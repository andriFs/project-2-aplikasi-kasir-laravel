<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Proses Install setelah download atau clone repository

```shell
# install composer-dependency
$ composer install
# install npm package
$ npm install
# build dev
$ npm run dev
# create copy of .env
$ cp .env.example .env
# create laravel key
$ php artisan key:generate
# create table
$ php artisan migrate
# create data seeder
$ php artisan db:seed
```

# Akun Login

User Admin <br>
email : admin@admin.app <br>
password : 12345678
<br>
<br>
User Kasir <br>
email : kasir@kasir.app <br>
password : 12345678
